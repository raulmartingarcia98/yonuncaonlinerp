
//DECLARACION DE VARIABLES
var btn = document.getElementById('btn');
var main = document.getElementsByClassName('main')[0];
var list = document.getElementsByClassName('list')[0];

var like = document.getElementsByClassName('like')[0];
var dislike = document.getElementsByClassName('dislike')[0];
var next = document.getElementsByClassName('next')[0];

var question = document.getElementsByClassName('question')[0];
var audio = new Audio('/sounds/boton.mp3');
var itemLists = document.getElementsByClassName('item');

var preguntas = [];
var numPregunta = null;
var categoria = "";

//JS DEL BOTON DE SALIR

var exit = document.getElementsByClassName('exit')[0].addEventListener('click', () => {
    location.reload();
});

//FIN DEL JS DEL BOTON DE SALIR

//JS DEL BOTON DE NEXT

next.addEventListener('click',() => {

    preguntaNueva();

});

//FIN JS DEL BOTON DE NEXT

//JS DE MOSTRAR PREGUNTA NUEVA

function preguntaNueva(){

    //Mostramos una pregunta y la eliminamos del array para que no se repita

    if (numPregunta != null)
        preguntas.splice(numPregunta,1);

    if (preguntas.length > 0){

        numPregunta = Math.floor(Math.random() * preguntas.length);
        var texto = preguntas[numPregunta].question;

        document.getElementById('question').innerText = texto;

    }else {
        document.getElementById('question').innerText = "¡¡Ya has jugado todas las preguntas!!";
    }

}

//FIN JS DE MOSTRAR PREGUNTA NUEVA

//JS DE BOTONES DE LIKE Y DISLIKE
like.addEventListener('click', () => {

    like.classList.toggle('active');
    console.log("Like a la pregunta en posicion: " + numPregunta);

});
dislike.addEventListener('click', () => {

    dislike.classList.toggle('active');
    console.log("Like a la pregunta en posicion: " + numPregunta);

});

//FIN JS DE BOTONES DE LIKE Y DISLIKE

//JS DE LA LISTA

itemLists[0].addEventListener('click', () => {

    accionItemList(0);

});
itemLists[1].addEventListener('click', () => {

    accionItemList(1);

});
itemLists[2].addEventListener('click', () => {

    accionItemList(2);

});
itemLists[3].addEventListener('click', () => {

    accionItemList(3);

});

function accionItemList(item) {
    
    audio.play();

    itemLists[item].classList.toggle('active');

    switch (item) {

        case 0:
            categoria = "hot";
            break;

        case 1:
            categoria = "funny";
            break;

        case 2:
            categoria = "drink";
            break;

        case 3:
            categoria = "mixed";
            break;

    }
    console.log("Categoria seleccionada: " + categoria);

    list.classList.toggle('hidden');

    question.style.display = "block";

    setTimeout(function () {
        list.style.display = "none";
        question.classList.toggle('hidden')
    }, 500); // <-- time in milliseconds

    //obtener todas las preguntas y mostrar una aleatoria
    fetch('/questions')
        .then(response => response.json())
        .then(json => {

            json.forEach(pregunta => preguntas.push(pregunta));
            preguntaNueva();

        });



}

//FIN JS DE LA LISTA

//JS DEL BOTON DE INICIO

btn.addEventListener('click', () => {

    audio.play();

    main.style.display = "flex";
    btn.classList.toggle('active');
    btn.classList.toggle('hidden');


    setTimeout(function () {
        btn.style.display = "none";
    }, 500); // <-- time in milliseconds

    setTimeout(function () {
        main.classList.toggle('hidden');
    }, 500); // <-- time in milliseconds

});

//FIN JS DEL BOTON DE INICIO

