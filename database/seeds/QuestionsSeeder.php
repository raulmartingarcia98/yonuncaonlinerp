<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'question' => '...he bebido alcohol con familiares',
            'category' => 'comon',
            'rating' => 0,
        ]);

        DB::table('questions')->insert([
            'question' => '...me he saltado clases en el instituto',
            'category' => 'comon',
            'rating' => 0,
        ]);

        DB::table('questions')->insert([
            'question' => '...me he caido hacia atrás con una silla',
            'category' => 'comon',
            'rating' => 0,
        ]);

        DB::table('questions')->insert([
            'question' => '...me he bañado en la piscina publica fuera de horarios a escondidas',
            'category' => 'comon',
            'rating' => 0,
        ]);
    }
}
