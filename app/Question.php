<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $table = 'questions';
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected $attributes = [
        'rating' => '0',
    ];

}
