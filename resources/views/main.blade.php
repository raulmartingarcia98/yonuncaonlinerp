<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169407091-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-169407091-1');
    </script>

    <!-- GOOGLE ADSENSE -->
    <script data-ad-client="ca-pub-8852831485627946" async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <meta charset="utf-8">

    <title>Yo Nunca Online</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Lexend+Tera&family=Ubuntu:wght@500&display=swap"
          rel="stylesheet">


    <link href="https://fonts.googleapis.com/css?family=Material+Icons+Round"
          rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Material+Icons"
          rel="stylesheet">

    <link rel="stylesheet" href="css/main.css">

</head>
<body>

<!--<span class="titulo">Yo Nunca Online</span>-->

<div class="exit">
    <i class="material-icons-round">exit_to_app</i>
</div>

<div class="btn" id="btn">
    <div class="inner-wrapper">
        <i class="material-icons-round">play_arrow</i>
    </div>
</div>

<div class="main hidden">

    <label class="gameLabel">Yo nunca...</label>

    <div class="list">
        <div class="item">
            <i class="material-icons">trending_up</i>
            <div style="display: flex; flex-direction: column">
                <label for="">Caliente</label>
                <label class="descripcion" for="">Descripción de la opción 1</label>
            </div>
        </div>
        <div class="item">
            <i class="material-icons">favorite</i>
            <div style="display: flex; flex-direction: column">
                <label for="">Gracioso</label>
                <label class="descripcion" for="">Descripción de la opción 2</label>
            </div>
        </div>
        <div class="item">
            <i class="material-icons">favorite</i>
            <div style="display: flex; flex-direction: column">
                <label for="">Emborrachate</label>
                <label class="descripcion" for="">Descripción de la opción 3</label>
            </div>
        </div>
        <div class="item">
            <i class="material-icons">favorite</i>
            <div style="display: flex; flex-direction: column">
                <label for="">Aleatorio</label>
                <label class="descripcion" for="">Descripción de la opción 4</label>
            </div>
        </div>
    </div>


    <div class="question hidden">
        <p id="question">...me he saltado clases en el instituto</p>

        <div class="botones">
            <div class="dislike"><i class="material-icons-round">thumb_down_alt</i></div>
            <div class="next"><i class="material-icons-round">double_arrow</i></div>
            <div class="like"><i class="material-icons-round">thumb_up_alt</i></div>
        </div>
    </div>

</div>

<div class="anuncio"></div>

<script src="js/animations.js"></script>


</body>
</html>
